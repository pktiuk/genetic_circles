# Genetic Circles
Simple app trying to recreate chosen picture by drawing multiple circles.
It utilizes simple genetic algorithm to do it.

![Example](./docs/Example.jpg)

# Dependencies
Required dependencies:
`cmake gcc g++ libsfml-dev`

Optional dependencies:

`libboost-all-dev` -for tests

`doxygen` - for building documentation



# Usage
Run using commandline
```
Usage:    ./evolving-circles [OPTIONS]
          -i  input file (if not defined program will show selection window)
          -p  population size (default 30)
          -g  gene pool, maximum number of circles on picture (default 1000)
          -c  end simulation after given amount of generations (empty or 0, no restriction)
```

# Duilding and testing
- download repository
```
git clone https://gitlab.com/pktiuk/genetic_circles.git
```

- download dependencies

```
sudo apt-get install cmake gcc g++ libsfml-dev libboost-all-dev doxygen
```
(last two are optional)
- build app
```
cd genetic_circles
sh ./build.sh
```
- run 
```
./bin/evolving-circles
```
- run tests 
```
sh ./run_tests.sh
```
- build documentation
```
sh ./build.sh -DBUILD_DOCS=TRUE 
```
index.html will be located in docs/thml

# Documentation site
[Link](https://pktiuk.gitlab.io/genetic_circles/index.html)

# Class Diagram
```mermaid
 classDiagram
      EvolAlg *-- Member
      EvolAlgObserver <|-- StatsObserver
      EvolAlg o-- EvolAlgObserver

      class EvolAlg{
        -curr_population: std::vector<Member>
        -original_image_: sf::Image
        -best_member_texture_: sf::Texture
        -observers_list_: std::list<EvolAlgObserver *>
        -population_size_: unsigned int
        -max_genes_: unsigned int
        -max_generations_: unsigned long long
        -notifyObservers()
        -reproduce()
        -crossover()
        -mutate()
        -deleteCircle(int pos, int index)
        -succesion()
        +EvolAlg(const std::string &filename, const unsigned int size, const unsigned int genes_count, const unsigned int max_generations = 0): 
        +loadInputImage(const std::string &filename)
        +run()
        +init()
        +stop()
        +addObserver(EvolAlgObserver *observer) 
        +getInputImage() const : const sf::Image
        +getBestMemberTexture() const : const sf::Texture
        +getBestFitness() const : unsigned long long
        +getGeneration() const : unsigned long long
        +isRunning() 
        +isImageLoaded() 
        +getPercentFitness(): double
        +getbestFitness() : unsigned long long
        +nextGeneration()
      }

      class Member{
        -fitness_: unsigned long long
        -image_x_size_: unsigned int
        -image_y_size_: unsigned int
        -max_circles_: unsigned int
        -distribution_: std::normal_distribution<float>
        +circles_: std::vector<sf::CircleShape>
        +Member(unsigned int max_genes, const unsigned int x, const unsigned int y): 
        +calculateFitness(const sf::Uint8 *original_pixels_ptr)
        +operator<(const Member &os) const
        +getTexture(): sf::Texture
        +addCircle()
        +deleteCircle(const unsigned int circle_index)
        +isOutOfBounds(const unsigned int circle_index)
        +mutateColor(const unsigned int circle_index)
        +mutatePosition(const unsigned int circle_index)
        +mutateShape(const unsigned int circle_index)
        +setModified(bool val)
        +isModified()
        +getFitness(): unsigned long long
      }

      class EvolAlgObserver{
        +update()
      }

      class StatsObserver{
        -observed_EvolAlg: EvolAlg*
        -clock: sf::Clock
        -first_generation_: unsigned long long
        -first_fitness_: unsigned long long
        -started
        -view_timer_: sf::Clock
        -refresh_time: int
        +setObservedEvolAlg(EvolAlg *p)
      }

```