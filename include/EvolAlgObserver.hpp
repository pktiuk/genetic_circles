/**
 * @file EvolAlgObserver.hpp
 * @author Paweł Kotiuk
 * @version 0.1
 * @date 2020-04-04
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef EVOL_ALG_OBSERVER
#define EVOL_ALG_OBSERVER

/**
 * @brief Interface used for observing EvolAlg
 */
class EvolAlgObserver
{
  protected:
  public:
    virtual void update() = 0;
};

#endif