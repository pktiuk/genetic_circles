/**
 * @file Member.hpp
 * @author Mateusz Chruściel
 * @brief File containing class Member
 * @version 0.1
 * @date 2020-03-24
 *
 * @copyright Copyright (c) 2020
 *
 */
#ifndef MEMBER_HPP_
#define MEMBER_HPP_
#include <SFML/Graphics.hpp>
#include <iostream>
#include <random>
#include <vector>

/**
 * @brief Member class represents one member of EvolAlg, holds vector for circles and fitness score
 *
 */
class Member
{
  private:
    unsigned long long fitness_;
    unsigned int image_x_size_;
    unsigned int image_y_size_;
    bool modified_ = false;
    unsigned int max_circles_;
    std::normal_distribution<float> distribution_;

  public:
    std::vector<sf::CircleShape> circles_;
    std::default_random_engine generator_;

    Member(unsigned int max_genes, const unsigned int x, const unsigned int y);
    void calculateFitness(const sf::Uint8 *original_pixels_ptr);
    bool operator<(const Member &os) const;
    sf::Texture getTexture();
    void addCircle();
    void deleteCircle(const unsigned int circle_index);
    bool isOutOfBounds(const unsigned int circle_index);
    void mutateColor(const unsigned int circle_index);
    void mutatePosition(const unsigned int circle_index);
    void mutateShape(const unsigned int circle_index);
    void setModified(bool val);
    bool isModified();
    unsigned long long getFitness();
};
#endif // !MEMBER_HPP_
