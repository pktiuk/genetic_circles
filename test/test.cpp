#define BOOST_TEST_MAIN
#if !defined(WIN32)
#define BOOST_TEST_DYN_LINK
#endif
#include "EvolAlg.hpp"
#include "Member.hpp"
#include <boost/test/test_tools.hpp>
#include <boost/test/unit_test.hpp>

namespace utf = boost::unit_test;

const std::string test_image_filename = "../MonaLisa.jpg";

BOOST_AUTO_TEST_CASE(adding_circles, *utf::label("no_x11"))
{
    std::cout << std::endl << "---Testing addCircle()---" << std::endl;
    Member m(1, 100, 100);
    std::cout << "Max circle size = 1" << std::endl;
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
    BOOST_TEST(m.circles_.size() == 0);
    BOOST_TEST(!m.isModified());
    std::cout << "adding circle" << std::endl;
    m.addCircle();
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
    BOOST_TEST(m.circles_.size() == 1);
    BOOST_TEST(m.isModified());
    m.setModified(false);
    std::cout << "adding circle over limit" << std::endl;
    m.addCircle();
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
    BOOST_TEST(m.circles_.size() == 1);
    BOOST_TEST(!m.isModified());
}
BOOST_AUTO_TEST_CASE(deleting_circle, *utf::label("no_x11"))
{
    std::cout << std::endl << "---Testing deleteCircle()---" << std::endl;
    Member m(1, 100, 100);
    std::cout << "Max circle size = 1" << std::endl;
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
    BOOST_TEST(m.circles_.size() == 0);
    std::cout << "adding circle" << std::endl;
    m.addCircle();
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
    std::cout << "deleting circle" << std::endl;
    m.deleteCircle(0);
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
    std::cout << "deleting non existing circle" << std::endl;
    m.deleteCircle(0);
    std::cout << "circle.size() = " << m.circles_.size() << std::endl;
}
BOOST_AUTO_TEST_CASE(calculating_fitness)
{
    std::cout << std::endl << "---Testing calculateFitness()---" << std::endl;
    // load image
    sf::Image image;

    image.loadFromFile(test_image_filename);
    const sf::Uint8 *image_ptr = image.getPixelsPtr();

    Member m(1, image.getSize().x, image.getSize().y);
    m.setModified(true);
    m.calculateFitness(image_ptr);

    const unsigned long long expected_fitness_before_drawing = 4216592814; // squared value of all pixels colors in MonaLisa.jpg

    // calculate fitness
    std::cout << "fit1: " << m.getFitness() << " excpected: " << expected_fitness_before_drawing << std::endl;
    BOOST_CHECK_EQUAL(expected_fitness_before_drawing, m.getFitness());

    // set seed to disable random circle generation
    srand(0);
    // draw circle
    m.addCircle();
    m.calculateFitness(image_ptr);
    // once more check fitness
    const unsigned long long expected_fitness_after_drawing = 4260554412;
    std::cout << "fit2: " << m.getFitness() << " excpected: " << expected_fitness_after_drawing << std::endl;
    BOOST_CHECK_EQUAL(expected_fitness_after_drawing, m.getFitness());
}
BOOST_AUTO_TEST_CASE(compare_opertor)
{
    sf::Image image;
    image.loadFromFile(test_image_filename);
    const sf::Uint8 *image_ptr = image.getPixelsPtr();

    std::cout << std::endl << "---Testing Member::opertor<()---" << std::endl;
    Member m1(1, 1, 1);
    Member m2(1, 1, 1);

    m2.addCircle();
    m2.calculateFitness(image_ptr);

    BOOST_CHECK_EQUAL(m1.getFitness(), 0);
    BOOST_CHECK_NE(m2.getFitness(), 0);

    BOOST_ASSERT(m1 < m2);
    std::cout << "Member(" << m1.getFitness() << ") < Member(" << m2.getFitness() << ")" << std::endl;
}
BOOST_AUTO_TEST_CASE(mutate_color, *utf::label("no_x11"))
{
    std::cout << std::endl << "---Testing Member::mutateColor()---" << std::endl;
    // expected integer value of color after mutation
    int int_color = 1903149184;
    Member m(1, 1, 1);

    // turn off randomness
    srand(0);
    m.generator_.seed(0);

    m.addCircle();
    m.mutateColor(0);
    sf::Color mutated_color = m.circles_.at(0).getFillColor();
    BOOST_TEST(mutated_color.toInteger() == int_color);
    std::cout << "Mutated color: " << mutated_color.toInteger() << " Expected color: " << int_color << std::endl;
}
BOOST_AUTO_TEST_CASE(mutate_position)
{
    std::cout << std::endl << "---Testing Member::mutatePosition()---" << std::endl;
    // expected position after mutation
    sf::Vector2i position(1551, 101);
    Member m(2, 10000, 10000);

    // turn off randomness
    srand(0);
    m.generator_.seed(0);
    m.addCircle();
    m.mutatePosition(0);

    sf::Vector2i mutated_position(m.circles_.at(0).getPosition().x, m.circles_.at(0).getPosition().y);
    BOOST_TEST(position.x == mutated_position.x);
    BOOST_TEST(position.y == mutated_position.y);

    std::cout << "Mutated position: " << mutated_position.x << "," << mutated_position.y << " Expected position: " << position.x << "," << position.y
              << std::endl;

    Member m2(1, 1, 1);
    m2.addCircle();
    std::cout << "circle.size(): " << m.circles_.size() << std::endl;
    BOOST_TEST(m2.circles_.size() == 1);
    std::cout << "mutating position of of view -> deleting circle" << std::endl;
    m2.mutatePosition(0);
    std::cout << "circle.size(): " << m.circles_.size() << std::endl;
    BOOST_TEST(m2.circles_.size() == 0);
}
BOOST_AUTO_TEST_CASE(mutate_shape, *utf::label("no_x11"))
{
    std::cout << std::endl << "---Testing Member::mutateShape()---" << std::endl;
    int expected_radius_before = 43;
    int expected_radius_after = 43;
    Member m(1, 10, 10);
    srand(0);
    m.generator_.seed(0);
    m.addCircle();
    int radius_before = m.circles_.at(0).getRadius();
    BOOST_TEST(radius_before == expected_radius_before);
    m.mutateShape(0);
    int mutated_radius = m.circles_.at(0).getRadius();
    BOOST_TEST(mutated_radius == expected_radius_after);
    std::cout << "Mutated radius: " << mutated_radius << " Expected radius: " << expected_radius_after << std::endl;
}
BOOST_AUTO_TEST_CASE(out_of_bounds_test)
{
    std::cout << std::endl << "---Testing Member::isOutOfBounds()---" << std::endl;
    srand(0);
    int size = 10;
    Member m(1, size, size);
    m.addCircle();
    int radius = m.circles_.at(0).getRadius();
    std::cout << "Radius: " << radius << " Window size: " << size << std::endl;
    std::cout << "Position: " << -radius << "," << 1 << std::endl;
    m.circles_.at(0).setPosition(-radius, 1);
    BOOST_TEST(m.isOutOfBounds(0));
    std::cout << "Position: " << 1 << "," << -radius << std::endl;
    m.circles_.at(0).setPosition(1, -radius);
    BOOST_TEST(m.isOutOfBounds(0));
    std::cout << "Position: " << 1 << "," << radius + size << std::endl;
    m.circles_.at(0).setPosition(1, radius + size);
    BOOST_TEST(m.isOutOfBounds(0));
    std::cout << "Position: " << radius + size << "," << 1 << std::endl;
    m.circles_.at(0).setPosition(radius + size, 1);
    BOOST_TEST(m.isOutOfBounds(0));
    std::cout << "Position: " << size / 2 << "," << size / 2 << std::endl;
    m.circles_.at(0).setPosition(size / 2, size / 2);
    BOOST_TEST(!m.isOutOfBounds(0));
}
BOOST_AUTO_TEST_CASE(default_run)
{
    std::cout << std::endl << "---Testing normal behavior---" << std::endl;
    int generations = 1000;
    EvolAlg alg(test_image_filename, 20, 100, generations);

    BOOST_TEST(alg.isImageLoaded());
    BOOST_TEST(alg.getGeneration() == 0);
    BOOST_TEST(alg.getbestFitness() == 0);

    alg.run();

    BOOST_TEST(!alg.isRunning());
    BOOST_TEST(alg.getbestFitness() != 0);
    BOOST_TEST(alg.getGeneration() == generations);
}
