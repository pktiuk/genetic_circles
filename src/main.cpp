/**
 * @file main.cpp
 * @author Paweł Kotiuk
 * @brief Main file containing main function and minor functions
 * @version 0.1
 * @date 2020-06-02
 *
 * @copyright Copyright (c) 2020
 *
 */

#include "EvolAlg.hpp"
#include "Member.hpp"
#include "StatsObserver.hpp"
#include <iostream>
#include <string.h>
#include <unistd.h>
#if _WIN32
#include <windows.h>
#endif

using std::cout;
/**
 * @brief Displays help for running program with different options
 *
 */
void printHelp()
{
    cout << "Usage:    evolving-circles [OPTIONS]" << std::endl;
    cout << "          -i  input file (if not described program will show selection window)" << std::endl;
    cout << "          -p  population size (default 30)" << std::endl;
    cout << "          -g  gene pool, maximum number of circles on picture (default 1000)" << std::endl;
    cout << "          -c  end simulation after given amount of generations (empty or 0, no restriction)" << std::endl;
    cout << "          -h  print help" << std::endl;
}

/**
 * @brief Opens File select dialog for current system
 *
 * @return std::string containing a path to selected file
 */
std::string openFileSelectDialog()
{
    std::string filename = "";
#if (unix)
    FILE *f = popen("zenity --file-selection", "r");
    char c;
    if (!f)
    {
        pclose(f);
        return "";
    }
    c = fgetc(f);
    if (c == -1)
        return "";
    while (c != '\n')
    {
        filename += c;
        c = fgetc(f);
    }
#elif _WIN32

    OPENFILENAME ofn; // common dialog box structure
    char szFile[250]; // buffer for file name
    HWND hwnd;        // owner window

    // Initialize OPENFILENAME
    ZeroMemory(&ofn, sizeof(ofn));
    ofn.lStructSize = sizeof(ofn);
    ofn.lpstrFile = szFile;
    ofn.lpstrFile[0] = '\0';
    ofn.nMaxFile = sizeof(szFile);
    ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

    // Display the Open dialog box.

    if (GetOpenFileName(&ofn) == false)
        return "";

    filename = szFile;
#endif

    return filename;
}
/**
 * @brief Main function, entry point of the program
 *
 * @param argc
 * @param argv
 * @return int
 */
int main(int argc, char *argv[])
{
    int option;
    int population_size = 30;
    int gene_count = 1000;
    int max_generations = 0;
    std::string filename = "";

    try
    {
        while ((option = getopt(argc, argv, ":i:p:g:hc:")) != -1)
        { // get option from the getopt() method
            switch (option)
            {
            case 'i':
                filename = optarg;
                break;
            case 'h':
                printHelp();
                return 0;
                break;

            case 'p':
                population_size = std::stoi(optarg);
                if (population_size <= 0)
                    throw("Population size must be greater than 0");
                break;
            case 'g':
                gene_count = std::stoi(optarg);
                if (gene_count <= 0)
                    throw("Gene count must be greater than 0");
                break;
            case 'c':
                max_generations = std::stoi(optarg);
                if (max_generations < 0)
                    throw("Generation count cannot be lower than 0");
                break;
            case ':':
                printf("option needs a value\n");
                return 1;
                break;
            case '?': // used for some unknown options
                printf("unknown option: %c\n", optopt);
                printHelp();
                return 1;
                break;
            }
        }
    } catch (const std::exception &e)
    {
        if (!strcmp(e.what(), "stoul"))
        {
            cout << "Wrong value of parameter -" << char(option) << std::endl;
        } else
        {
            cout << "Unknown exception occured: " << e.what() << std::endl;
        }
        printHelp();
        return -1;
    } catch (const char *msg)
    {
        std::cout << msg << std::endl;
        return -1;
    }

    if (filename == "")
    {
        filename = openFileSelectDialog();
        if (filename == "")
        {
            std::cout << "File not selected\n";
            return -1;
        }
    }

    cout << "Input file: " << filename;
    cout << "\nPopulation size: " << population_size;
    cout << "\nMaximal number of circles: " << gene_count << std::endl;

    EvolAlg p(filename, population_size, gene_count, max_generations);

    StatsObserver o;
    o.setObservedEvolAlg(&p);
    srand(time(NULL));
    p.run();
    return 0;
}
