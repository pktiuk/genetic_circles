/**
 * @file Member.cpp
 * @author Mateusz Chruściel
 * @brief
 * @version 0.1
 * @date 2020-03-24
 *
 * @copyright Copyright (c) 2020
 *
 */
#include "Member.hpp"

/**
 * @brief Construct a new Member:: Member object
 *
 * @param max_genes - maximal number of circles in picture
 * @param x - width of image
 * @param y -height of image
 */
Member::Member(unsigned int max_genes, unsigned int x, unsigned int y)
    : fitness_(0)
    , image_x_size_(x)
    , image_y_size_(y)
    , max_circles_(max_genes)
    , distribution_(0, 4)
{
    circles_.reserve(max_genes);
}

/**
 * @brief Calculate fitness of Member's image. Squared difference between pixels value. Lower fitness is better.
 *
 * @param original_pixels_ptr -pointer to imput image
 */
void Member::calculateFitness(const sf::Uint8 *const original_pixels_ptr)
{
    if (modified_) // skip if nothing changed
    {
        unsigned long fitness_local = 0;
        sf::RenderTexture generated_texture;
        generated_texture.create(image_x_size_, image_y_size_);
        generated_texture.clear();
        for (auto &circle : circles_)
            generated_texture.draw(circle);
        generated_texture.display();

        const auto image = generated_texture.getTexture().copyToImage();
        const sf::Uint8 *generated_pixels_ptr = image.getPixelsPtr();
        if (!generated_pixels_ptr || !original_pixels_ptr)
            std::cout << "blad wskaznikow" << std::endl;

        for (unsigned int i = 0; i < image_x_size_ * image_y_size_ * 4; i += 4)
        {
            fitness_local += (original_pixels_ptr[i + 0] - generated_pixels_ptr[i + 0]) * (original_pixels_ptr[i + 0] - generated_pixels_ptr[i + 0]) +
                             (original_pixels_ptr[i + 1] - generated_pixels_ptr[i + 1]) * (original_pixels_ptr[i + 1] - generated_pixels_ptr[i + 1]) +
                             (original_pixels_ptr[i + 2] - generated_pixels_ptr[i + 2]) * (original_pixels_ptr[i + 2] - generated_pixels_ptr[i + 2]);
        }
        fitness_ = fitness_local;
        modified_ = false;
    }
}

/**
 * @brief compare fitness of two Members
 *
 * @param os
 * @return true
 * @return false
 */
bool Member::operator<(const Member &os) const { return fitness_ < os.fitness_; }

/**
 * @brief Returns sf::Texture for given EvolAlg member
 *
 * @return sf::Texture
 */
sf::Texture Member::getTexture()
{
    sf::RenderTexture generated_texture;
    generated_texture.create(image_x_size_, image_y_size_);
    generated_texture.clear();

    for (auto &circle : circles_)
        generated_texture.draw(circle);

    generated_texture.display();
    return generated_texture.getTexture();
}

/**
 * @brief Add new circle to model describing Member
 *
 */
void Member::addCircle()
{
    if (circles_.size() < max_circles_)
    {
        sf::CircleShape circle(rand() % 50 + 10, 20);
        circle.setFillColor(sf::Color(rand() % 256, rand() % 256, rand() % 256, 128));
        circle.setPosition(rand() % (image_x_size_ + 20) - 10, rand() % (image_y_size_ + 20) - 10);
        circles_.push_back(circle);
        modified_ = true;
    }
}
/**
 * @brief Calculate if the circle moved out of the picture and is no longer visible
 *
 * @param circle_index
 * @return true
 * @return false
 */
bool Member::isOutOfBounds(const unsigned int circle_index)
{
    return (circles_.at(circle_index).getPosition().x - circles_.at(circle_index).getRadius() >= image_x_size_ ||
            circles_.at(circle_index).getPosition().x + circles_.at(circle_index).getRadius() <= 0 ||
            circles_.at(circle_index).getPosition().y - circles_.at(circle_index).getRadius() >= image_y_size_ ||
            circles_.at(circle_index).getPosition().y + circles_.at(circle_index).getRadius() <= 0);
}
/**
 * @brief Delete circle at given index
 *
 * @param circle_index
 */
void Member::deleteCircle(const unsigned int circle_index)
{
    if (circles_.size() > 0 && circle_index < circles_.size())
    {
        std::swap(circles_.at(circle_index), circles_.at(circles_.size() - 1));
        circles_.pop_back();
        modified_ = true;
    }
}
/**
 * @brief Randomly change color of circle at given index
 *
 * @param circle_index
 */
void Member::mutateColor(const unsigned int circle_index)
{
    sf::Color tmp_color = circles_.at(circle_index).getFillColor();
    tmp_color.r += distribution_(generator_);
    tmp_color.g += distribution_(generator_);
    tmp_color.b += distribution_(generator_);
    circles_.at(circle_index).setFillColor(tmp_color);
    modified_ = true;
}
/**
 * @brief Randomly change position of circle at given index. Circle is deleted when moves out of view
 *
 * @param circle_index
 */
void Member::mutatePosition(const unsigned int circle_index)
{
    circles_.at(circle_index).move(distribution_(generator_), distribution_(generator_));
    modified_ = true;
    if (isOutOfBounds(circle_index))
        deleteCircle(circle_index);
}
/**
 * @brief randomly change radius of circle at given index. Circle is deleted when it becomes too small
 *
 * @param circle_index
 */
void Member::mutateShape(const unsigned int circle_index)
{
    float tmp_rand = distribution_(generator_) / 3.0;
    circles_.at(circle_index).scale(tmp_rand, tmp_rand);
    modified_ = true;
    // if circle becomes too small delete it
    if (circles_.at(circle_index).getRadius() < 3)
        deleteCircle(circle_index);
}

bool Member::isModified() { return modified_; }
void Member::setModified(bool val) { modified_ = val; }
unsigned long long Member::getFitness() { return fitness_; }