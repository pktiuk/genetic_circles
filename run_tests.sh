#!/usr/bin/env bash



sh ./build.sh -DBUILD_TESTS=TRUE > /dev/null

cd build
if [ $1 ]
then
    if [ $1 = "-v" ]
    then
    ./basic_test --color_output
    return $?
    fi
    if [ $1 = "-r" ]
    then
    ./basic_test --color_output --run_test=@no_x11
    return $?
    fi
else
ctest -Q
fi